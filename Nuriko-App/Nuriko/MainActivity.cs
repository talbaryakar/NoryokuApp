﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Content;

namespace Nuriko
{
    [Activity(Label = "סוד הקסם היפני", Theme = "@style/Mytheme.Splash", MainLauncher = true, NoHistory = true)]
    public class MainActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            //Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            //SetContentView(Resource.Layout.activity_main);
        }
        /*
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        */
        protected override void OnResume()
        {
            base.OnResume();
            System.Threading.Thread.Sleep(4000);
            Intent intent = new Intent(this, typeof(Activities.LogOnOptionsActivity));
            StartActivity(intent);
        }
    }
}