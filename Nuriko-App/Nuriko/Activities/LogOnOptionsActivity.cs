﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Nuriko.Classes;

namespace Nuriko.Activities
{
   
    [Activity(Label = "אפשרויות כניסה")]
    
    public class LogOnOptionsActivity : AppCompatActivity
    {
        
        Button SignIn, SignUp, SignInAsAdminister;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.LogOnOptions);
            SignIn = (Button)FindViewById(Resource.Id.SignInButton);
            SignUp = (Button)FindViewById(Resource.Id.SignUpButton);
            SignInAsAdminister = (Button)FindViewById(Resource.Id.SignInAdminister);
            SignIn.Click += SignIn_Click;
            SignUp.Click += SignUp_Click;
            SignInAsAdminister.Click += SignInAsAdminister_Click;
        }

        private void SignInAsAdminister_Click(object sender, EventArgs e)
        {
            Fragments.SignInAsAdministerFragment AsAdminister = new Fragments.SignInAsAdministerFragment();
            var trans = SupportFragmentManager.BeginTransaction();
            AsAdminister.Show(trans, "wrong");
        }

        private void SignUp_Click(object sender, EventArgs e)
        {
            moveToSignUp();
        }

        private void SignIn_Click(object sender, EventArgs e)
        {
            Fragments.SignInFragment newFragment = new Fragments.SignInFragment();
            newFragment.notifier += moveToSignUp;
            newFragment.notifierMainMenuUser += moveToMainMenuForUser;
            var trans = SupportFragmentManager.BeginTransaction();
            newFragment.Show(trans, "wrong");
        }
        private void moveToSignUp()
        {
            Fragments.SignUpFragment signUpPage = new Fragments.SignUpFragment();
            var trans = SupportFragmentManager.BeginTransaction();
            signUpPage.Show(trans, "wrong");
        }
        private void moveToMainMenuForUser(string user)
        {
            Intent intent = new Intent(this, typeof(Activities.mainMenuUserActivity));
            intent.PutExtra("user",user);
            StartActivity(intent);
        }
    }
}