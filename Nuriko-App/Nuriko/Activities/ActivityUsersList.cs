﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json.Linq;
using Nuriko.Adapters;
using Nuriko.Classes;

namespace Nuriko.Activities
{
    [Activity(Label = "ActivityUsersList")]
    public class ActivityUsersList : AppCompatActivity
    {
        bool isProblematicUsers = true;
        RecyclerView recycleView;
        AdapterUsers userAdapter;
        List<User> userList;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            SetContentView(Resource.Layout.layoutDisplayAllUsers);

            string extra = Intent.GetStringExtra("problem");
            isProblematicUsers = extra == "Yes" ?  true:false;
            recycleView = (RecyclerView)FindViewById(Resource.Id.userRecycleView);
            if (isProblematicUsers == true)
                userList = getAllProblematicUser();
            else
                userList = getUsersFromServer();
            setupRecyclerView();
        }
        void setupRecyclerView()
        {
            recycleView.SetLayoutManager(new LinearLayoutManager(recycleView.Context));
            userAdapter = new AdapterUsers(userList);
            userAdapter.ItemClick += UserAdapter_ItemClick;
            userAdapter.callClick += UserAdapter_callClick;
            userAdapter.emailClick += UserAdapter_emailClick;
            userAdapter.whatsAppClick += UserAdapter_whatsAppClick;
            userAdapter.displayHistoryClick += UserAdapter_displayHistoryClick;
            recycleView.SetAdapter(userAdapter);
        }

        private void UserAdapter_displayHistoryClick(object sender, AdapterUsersClickEventArgs e)
        {
            var User = userList[e.Position];
            Intent intent = new Intent(this, typeof(Activities.ActivityUserMonthlyDetails));
            intent.PutExtra("userName", User.UserName);
            StartActivity(intent);
        }

        private void UserAdapter_whatsAppClick(object sender, AdapterUsersClickEventArgs e)
        {
            var User = userList[e.Position];
            Android.Support.V7.App.AlertDialog.Builder EmailAlert = new Android.Support.V7.App.AlertDialog.Builder(this);
            EmailAlert.SetMessage("Send WhatsApp to :" + User.UserName);
            EmailAlert.SetPositiveButton("Se nd", (alert, args) =>
            {
                
                if (User.phoneNumber != null && User.phoneNumber.Length > 2)
                {
                    if (User.phoneNumber[0] == '0' && User.phoneNumber[1] == '5')
                    {
                        var whatsaap = Android.Net.Uri.Parse("smsto:" + User.phoneNumber);
                        Intent i = new Intent(Intent.ActionSendto, whatsaap);
                        i.SetPackage("com.whatsapp");
                        StartActivity(Intent.CreateChooser(i, ""));
                    }
                }
            });
            EmailAlert.SetNegativeButton("Cancle", (alert, args) =>
            {
                EmailAlert.Dispose();
            });
            EmailAlert.Show();
        }

        private void UserAdapter_emailClick(object sender, AdapterUsersClickEventArgs e)
        {
            var User = userList[e.Position];
            Android.Support.V7.App.AlertDialog.Builder EmailAlert = new Android.Support.V7.App.AlertDialog.Builder(this);
            EmailAlert.SetMessage("Send Mail to :" + User.UserName);
            EmailAlert.SetPositiveButton("Send", (alert, args) =>
             {
                 
                 Intent intent = new Intent();
                 intent.SetType("plain/text");
                 intent.SetAction(Intent.ActionSend);
                 intent.PutExtra(Android.Content.Intent.ExtraEmail, new string[] { User.emailAdress });
                 intent.PutExtra(Intent.ExtraSubject, "hey its nimrod");
                 StartActivity(intent);
             });
            EmailAlert.SetNegativeButton("Cancle", (alert, args) =>
            {
                EmailAlert.Dispose();
            });
            EmailAlert.Show();
        }

        private void UserAdapter_callClick(object sender, AdapterUsersClickEventArgs e)
        {
           
           
            var User = userList[e.Position];
            Android.Support.V7.App.AlertDialog.Builder CallAlerts = new Android.Support.V7.App.AlertDialog.Builder(this);
            CallAlerts.SetMessage("Call " + User.UserName);
            CallAlerts.SetPositiveButton("Call", (alert, args) =>
             {
                 var uri = Android.Net.Uri.Parse("tel:" + User.phoneNumber);
                 var intent = new Intent(Intent.ActionDial, uri);
                 StartActivity(intent);
             });
            CallAlerts.SetNegativeButton("Cancle", (alert, args) =>
             {
                 CallAlerts.Dispose();
             });
            CallAlerts.Show();
           
        }

        private void UserAdapter_ItemClick(object sender, AdapterUsersClickEventArgs e)
        {

            Toast.MakeText(this, "row was clicked", ToastLength.Long).Show();
        }
        private List<User> getUsersFromServer()
        {
            List<User> listOfUsers = new List<User>();
            string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/getAllUsersFromAdmin";
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            string responseAsString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (responseAsString != null)
            {
                JArray obj = JArray.Parse(responseAsString);
                foreach (JToken jt in obj)
                {
                    listOfUsers.Add((User)jt.ToObject(typeof(User)));
                }
                return listOfUsers;
               
            }
            return null;
        }
        private List<User> getAllProblematicUser()
        {
            List<User> listOfUsers = new List<User>();
            string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/getAllProblematicUsersFromAdmin";
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            string responseAsString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (responseAsString != null)
            {
                JArray obj = JArray.Parse(responseAsString);
                foreach (JToken jt in obj)
                {
                    listOfUsers.Add((User)jt.ToObject(typeof(User)));
                }
                return listOfUsers;

            }
            return null;
        }
    }
}//userRecycleView