﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nuriko.Classes;
using Nuriko.Fragments;

namespace Nuriko.Activities
{
    [Activity(Label = "mainMenuUserActivity")]
    public class mainMenuUserActivity : AppCompatActivity
    {
        User userObj;
        Button submitButton;
        LinearLayout water, weight, happines, sportActivity, solvescrisis, meals, tommorowPlanes, newRecord;
        TextView tVWater, tVWeight, tVHappines, tVSportActivity, tVSolvescrisis, tVMeals, tVTommorowPlanes, tVNewRecord;
        bool isDoneHappiness = false, isDoneSportActivity = false, isDoneSolveCrisis = false, isDoneTommorowPlans = false, isDoneNewRecord=false;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.MainUserLayout);
            water = (LinearLayout)FindViewById(Resource.Id.LinearLayoutWater);
            weight = (LinearLayout)FindViewById(Resource.Id.LinearLayoutWeightSclae);
            happines = (LinearLayout)FindViewById(Resource.Id.LinearLayoutHappiness);
            sportActivity = (LinearLayout)FindViewById(Resource.Id.LinearLayoutSportActivity);
            solvescrisis = (LinearLayout)FindViewById(Resource.Id.LinearLayoutSolvingCrisis);
            meals = (LinearLayout)FindViewById(Resource.Id.LinearLayoutMeals);
            tommorowPlanes = (LinearLayout)FindViewById(Resource.Id.LinearLayoutPlanningTomorrow);
            newRecord = (LinearLayout)FindViewById(Resource.Id.LinearLAyoutNewRecord);
            submitButton = (Button)FindViewById(Resource.Id.ButtonSubmitt); 
            tVWater = (TextView)FindViewById(Resource.Id.TextViewWater);
            tVWeight = (TextView)FindViewById(Resource.Id.TextViewWeightScale);
            tVHappines = (TextView)FindViewById(Resource.Id.TextviewHappiness);
            tVSolvescrisis = (TextView)FindViewById(Resource.Id.TextViewSolvingCrisis);
            tVSportActivity = (TextView)FindViewById(Resource.Id.TextViewSportActivity);
            tVMeals = (TextView)FindViewById(Resource.Id.TextViewMeals);
            tVTommorowPlanes = (TextView)FindViewById(Resource.Id.TextViewTommorowPlan);
            tVNewRecord = (TextView)FindViewById(Resource.Id.TextViewNewRecord);
            water.Click += Water_Click;
            meals.Click += Meals_Click;
            tommorowPlanes.LongClick += TommorowPlanes_LongClick;
            meals.LongClick += Meals_LongClick;
            weight.LongClick += Weight_LongClick;
            happines.LongClick += Happines_LongClick;
            water.LongClick += Water_LongClick;
            solvescrisis.LongClick += Solvescrisis_LongClick;
            newRecord.LongClick += NewRecord_LongClick;
            sportActivity.LongClick += SportActivity_LongClick;
            sportActivity.Click += SportActivity_Click;
            newRecord.Click += NewRecord_Click;
            tommorowPlanes.Click += TommorowPlanes_Click;
            happines.Click += Happines_Click;
            solvescrisis.Click += Solvescrisis_Click;
            submitButton.Click += SubmitButton_Click;
            string userAsString =Intent.GetStringExtra("user");
            JObject obj = JObject.Parse(userAsString);
            userObj = new User(obj);
            updteViewViaUserDetails(userAsString);
           
        }

        private void Weight_LongClick(object sender, View.LongClickEventArgs e)
        {
            FragmentWeight fragment = new FragmentWeight();
            fragment.updateWeightDelegate += setWeight;
            int weight;
            if (int.TryParse(tVWeight.Text, out weight))
            {
                fragment.destinationWeight = weight;
                fragment.currentWeight = weight;
            }
            else
            {
                fragment.destinationWeight = 0;
                fragment.currentWeight = 0;
            }
            
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void TommorowPlanes_LongClick(object sender, View.LongClickEventArgs e)
        {
            FragmentPlanningTomorrow fragment = new FragmentPlanningTomorrow();
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void Happines_LongClick(object sender, View.LongClickEventArgs e)
        {
            FragmentHappiness fragment = new FragmentHappiness();
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void Solvescrisis_LongClick(object sender, View.LongClickEventArgs e)
        {
            FragmentCrisisSolution fragment = new FragmentCrisisSolution();
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void NewRecord_LongClick(object sender, View.LongClickEventArgs e)
        {
            NewRecordFragment fragment = new NewRecordFragment();
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void SportActivity_LongClick(object sender, View.LongClickEventArgs e)
        {
            FragmentSportActivity fragment = new FragmentSportActivity();
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void Meals_LongClick(object sender, View.LongClickEventArgs e)
        {
            FragmentMealsDetails fragment = new FragmentMealsDetails();
            fragment.amountOfMealsDelegate += setAmountOfMealsDelegateMethood;
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void Meals_Click(object sender, EventArgs e)
        {
            tVMeals.Text = (int.Parse(tVMeals.Text) + 1).ToString();
        }

        private void SubmitButton_Click(object sender, EventArgs e)
        {
            string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/updateUserMissionsByDate";
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            updateUserFromView();
            string jsonString = JsonConvert.SerializeObject(userObj);
            byte[] bytes = Encoding.UTF8.GetBytes(jsonString);
            request.ContentLength = bytes.Length;
            request.ContentLength = bytes.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Flush();
                requestStream.Close();
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string line = reader.ReadToEnd();
                        if (line == null || line == "" || line == string.Empty)
                        {
                            Toast.MakeText(this, "אירעה בעיה ", ToastLength.Long).Show();
                        }
                        else
                        {
                            Toast.MakeText(this, "מידע עודכן ", ToastLength.Long).Show();
                        }
                    }


                }
            }

        }
        public void setAmountOfWaterDelegateMethood(int amount)
        {
            tVWater.Text = amount.ToString();
        }
        public void setWeight(int dest, int curr)
        {

            tVWeight.Text= dest==0?"לא עודכן": dest.ToString();
        }
        public void setAmountOfMealsDelegateMethood(int amount)
        {
            tVMeals.Text = amount.ToString();
        }
        public void updteViewViaUserDetails(string user)
        {
            tVWater.Text = userObj.missionsByDate[0].amountOfWater.ToString();
            tVWeight.Text = userObj.missionsByDate[0].targerWeight.ToString();
            changeState(tVHappines, ref userObj.missionsByDate[0].isHappinesCircle);
            changeState(tVSolvescrisis, ref  userObj.missionsByDate[0].isCrisisManageSuccufully);
            changeState(tVSportActivity, ref userObj.missionsByDate[0].isSportActivityDone);            
            tVMeals.Text = userObj.missionsByDate[0].amountOfMeals.ToString();
            changeState(tVTommorowPlanes, ref userObj.missionsByDate[0].isTommorowPlanned);
            changeState(tVNewRecord, ref userObj.missionsByDate[0].isNewRecord);
        }
        public void updateUserFromView()
        {

            userObj.missionsByDate[0].amountOfWater =int.Parse( tVWater.Text);
            userObj.missionsByDate[0].targerWeight =double.Parse( tVWeight.Text) ;
            userObj.missionsByDate[0].amountOfMeals =int.Parse(tVMeals.Text);          
        }

        private void Solvescrisis_Click(object sender, EventArgs e)
        {
            changeState(tVSolvescrisis,ref isDoneSolveCrisis);
        }

        private void Happines_Click(object sender, EventArgs e)
        {
            changeState(tVHappines,ref isDoneHappiness);
        }

        private void TommorowPlanes_Click(object sender, EventArgs e)
        {
            changeState(tVTommorowPlanes,ref isDoneTommorowPlans);
        }

        private void NewRecord_Click(object sender, EventArgs e)
        {
            changeState(tVNewRecord,ref isDoneNewRecord);
        }

        private void SportActivity_Click(object sender, EventArgs e)
        {
            changeState(tVSportActivity,ref isDoneSportActivity);
        }

        private void Water_LongClick(object sender, View.LongClickEventArgs e)
        {
            FragmentWaterDetails fragment = new FragmentWaterDetails();
            fragment.amountOfWaterDelegate += setAmountOfWaterDelegateMethood;
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");
        }

        private void Water_Click(object sender, EventArgs e)
        {
            tVWater.Text = (int.Parse(tVWater.Text) + 1).ToString();
        }
        private void changeState(TextView io_textView, ref bool io_isDone)
        {
            if (io_isDone)
            {
                io_textView.Text = "עשיתי";
                io_textView.SetTextColor(Android.Graphics.Color.Green);
                io_isDone = false;
            }
            else
            {
                io_textView.Text = "לא עשיתי";
                io_textView.SetTextColor(Android.Graphics.Color.Red);
                io_isDone = true;
            }
        }
    }
}