﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using Microcharts;
using Microcharts.Droid;
using Newtonsoft.Json.Linq;
using Nuriko.Classes;
using SkiaSharp;

namespace Nuriko.Activities
{
    [Activity(Label = "ActivityUserMonthlyDetails")]
    public class ActivityUserMonthlyDetails : AppCompatActivity
    {
        string[] monthNames = { " January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };   
        ChartView chartViewCrisis, chartViewHappy, chartViewSport , chartViewRecord, chartViewMeals, chartViewWieight, chartViewWater, chartViewTommorow;
        TextView meals, weight, water, sport, record, happy, crisis, tommorowManagment,UserMissionTV , MonthMissionTV;
        User userObj;
        List<Microcharts.ChartEntry> sportDataList = new List<Microcharts.ChartEntry>();
        List<Microcharts.ChartEntry> HappyDataList = new List<Microcharts.ChartEntry>();
        List<Microcharts.ChartEntry> recordDataList = new List<Microcharts.ChartEntry>();
        List<Microcharts.ChartEntry> tommorowDataList = new List<Microcharts.ChartEntry>();
        List<Microcharts.ChartEntry> crisisDataList = new List<Microcharts.ChartEntry>();
        List<Microcharts.ChartEntry> wightDataList = new List<Microcharts.ChartEntry>();
        List<Microcharts.ChartEntry> waterDataList = new List<Microcharts.ChartEntry>();
        List<Microcharts.ChartEntry> mealsDataList = new List<Microcharts.ChartEntry>();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layoutDetails);

            UserMissionTV = (TextView)FindViewById(Resource.Id.UserNameMonthlyMissions);
            MonthMissionTV = (TextView)FindViewById(Resource.Id.todayUserMission);
            meals = (TextView)FindViewById(Resource.Id.currentMealsTVuserMissionToday);
            water = (TextView)FindViewById(Resource.Id.currentWaterTVuserMissionToday);
            weight = (TextView)FindViewById(Resource.Id.currentWeightTVuserMissionToday);
            sport = (TextView)FindViewById(Resource.Id.currentSportActivityTVuserMissionToday);
            happy = (TextView)FindViewById(Resource.Id.currentHappinesTVuserMissionToday);
            record = (TextView)FindViewById(Resource.Id.currentNewRecordTVuserMissionToday);
            tommorowManagment = (TextView)FindViewById(Resource.Id.currentTommorowPlanedTVuserMissionToday);
            crisis = (TextView)FindViewById(Resource.Id.currentCrisisManagmentTVuserMissionToday);

            chartViewCrisis = (ChartView)FindViewById(Resource.Id.monthlyCrisisManagmentChart);
            chartViewTommorow = (ChartView)FindViewById(Resource.Id.monthlyTommorowPlannedChart);
            chartViewHappy = (ChartView)FindViewById(Resource.Id.monthlyHappinessChart);
            chartViewRecord = (ChartView)FindViewById(Resource.Id.monthlyNewRecordChart);
            chartViewSport = (ChartView)FindViewById(Resource.Id.monthlySportChart);
            chartViewMeals = (ChartView)FindViewById(Resource.Id.monthlyMealsChartDrinking);
            chartViewWater = (ChartView)FindViewById(Resource.Id.monthlyWaterDrinking);
            chartViewWieight = (ChartView)FindViewById(Resource.Id.monthlyweight);


            fillAllUserDetails();

            //------------------------

      
        }
        public void fillAllUserDetails()
        {
            
            MonthMissionTV.Text = monthNames[DateTime.Now.Month-1]+ "Missions";
            getAllUserDetailsFromServer();
           if (userObj==null)
           {

                Toast.MakeText(this, "אירעה בעיה ", ToastLength.Long).Show();
           }
           else
            {
                List<UserMissions> userMissions = userObj.missionsByDate;
                setCurrentDate(userMissions[0]);
                setAllCharts(userMissions);
            }
        }
        private void setCurrentDate(UserMissions userMission)
        {
            meals.Text = "Amount Of Meals :" + userMission.amountOfMeals;
            water.Text = "Amount of water: " + userMission.amountOfWater;
            weight.Text = "currnt weight: " + userMission.currentWeight;
            sport.Text ="Sport activity: " +(userMission.isSportActivityDone == true ? "done" : "not done yet");
            record.Text = "New record: " +( userMission.isNewRecord == true ? "Achived" : "not Achived yet");
            happy.Text = "Happiness cycle: " + (userMission.isHappinesCircle == true ? "done" : "not done yet");
            tommorowManagment.Text = "tommorow planning : " + (userMission.isTommorowPlanned == true ? "Done" : "not done yet");
            crisis.Text = "Crisis managment: " + (userMission.isCrisisManageSuccufully == true ? "Solved" : "not solved yet");
        }
        private void setAllCharts(List<UserMissions> userMissions)
        {
            userMissions.Reverse();
            UserMissions FirstMission= userMissions.Last();
            UserMissions LastMission = userMissions.First();
            int startDay = FirstMission.dateDay;
            int endDate = LastMission.dateDay;
            for (int i = 1; i < DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month); i++)
            {
                if (i == startDay)
                {
                    foreach (UserMissions missions in userMissions)
                    {
                        setChartsEntry(missions);
                        i++;
                    }
                }
                UserMissions dummyMission = new UserMissions()
                {
                    isSportActivityDone = false,
                    isCrisisManageSuccufully = false,
                    isHappinesCircle = false,
                    isNewRecord = false,
                    isTommorowPlanned = false,
                    currentWeight = 0,
                    amountOfMeals = 0,
                    amountOfWater = 0,
                    date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, i)
                };
                setChartsEntry(dummyMission, true);

            }
            var sportChart = new BarChart() { Entries = sportDataList, LabelTextSize = 30f ,MaxValue=1,MinValue=0};
            
            chartViewSport.Chart = sportChart;


            var crisisChart = new BarChart() { Entries = crisisDataList, LabelTextSize = 30f, MaxValue = 1, MinValue = 0 };
            chartViewCrisis.Chart = crisisChart;


            var happyChart = new BarChart() { Entries = HappyDataList, LabelTextSize = 30f , MaxValue = 1, MinValue = 0 };
            chartViewHappy.Chart = happyChart;


            var tommorowChart = new BarChart() { Entries = tommorowDataList, LabelTextSize = 30f , MaxValue = 1, MinValue = 0 };
            chartViewTommorow.Chart = tommorowChart;


            var recordChart = new BarChart() { Entries = recordDataList, LabelTextSize = 30f , MaxValue = 1, MinValue = 0 };
            chartViewRecord.Chart = recordChart;


            var weightChart = new BarChart() { Entries = wightDataList, LabelTextSize = 30f };
            chartViewWieight.Chart = weightChart;


            var mealsChart = new BarChart() { Entries = mealsDataList, LabelTextSize = 30f };
            chartViewMeals.Chart = mealsChart;


            var waterChart = new BarChart() { Entries = waterDataList, LabelTextSize = 30f };
            chartViewWater.Chart = waterChart;

        }
        private void setChartsEntry(UserMissions userMission, bool isDummy=false)
        {
            
            sportDataList.Add(new ChartEntry((userMission.isSportActivityDone == true ? 1 : 0) + (isDummy == true?0f :0.1f))
            {
                
                Label = userMission.dateDay.ToString() + @"\" + userMission.date.Month.ToString(),
                ValueLabel = isDummy==true?string.Empty: userMission.isSportActivityDone == true ? "Done" : "Not Done",
                Color = userMission.isSportActivityDone == true? SKColor.Parse("#54ff71"): SKColor.Parse("#c8241B"),
                TextColor = userMission.isSportActivityDone == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                ValueLabelColor = userMission.isSportActivityDone == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
            }) ;
            crisisDataList.Add(new ChartEntry(userMission.isCrisisManageSuccufully == true ? 1 : 0+(isDummy == true ? 0f : 0.1f))
            {
                Label = userMission.dateDay.ToString() + @"\"+ userMission.date.Month.ToString(),
                ValueLabel = isDummy == true ? string.Empty : userMission.isCrisisManageSuccufully == true ? "Solved" : "Not Solved",
                Color = userMission.isCrisisManageSuccufully == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                TextColor = userMission.isCrisisManageSuccufully == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                ValueLabelColor = userMission.isCrisisManageSuccufully == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
            });
            HappyDataList.Add(new ChartEntry(userMission.isHappinesCircle == true ? 1 : 0 + (isDummy == true ? 0f : 0.1f))
            {
                Label = userMission.dateDay.ToString() + @"\" + userMission.date.Month.ToString(),
                ValueLabel = isDummy == true ? string.Empty : userMission.isHappinesCircle == true ? "Done" : "Not Done",
                Color = userMission.isHappinesCircle == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                TextColor = userMission.isHappinesCircle == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                ValueLabelColor = userMission.isHappinesCircle == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
            });
            tommorowDataList.Add(new ChartEntry(userMission.isTommorowPlanned == true ? 1 : 0 + (isDummy == true ? 0f : 0.1f))
            {
                Label = userMission.dateDay.ToString() + @"\" + userMission.date.Month.ToString(),
                ValueLabel = isDummy == true ? string.Empty : userMission.isTommorowPlanned == true ? "Planned" : "Not Planned",
                Color = userMission.isTommorowPlanned == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                TextColor = userMission.isTommorowPlanned == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                ValueLabelColor = userMission.isTommorowPlanned == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
            });
            recordDataList.Add(new ChartEntry(userMission.isNewRecord == true ? 1 : 0 + (isDummy == true ? 0f : 0.1f))
            {
                Label = userMission.dateDay.ToString() + @"\" + userMission.date.Month.ToString(),
                ValueLabel = isDummy == true ? string.Empty : userMission.isNewRecord == true ? "Achived" : "Not Achived",
                Color = userMission.isNewRecord == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                TextColor = userMission.isNewRecord == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
                ValueLabelColor = userMission.isNewRecord == true ? SKColor.Parse("#54ff71") : SKColor.Parse("#c8241B"),
            });
            mealsDataList.Add(new ChartEntry(userMission.amountOfMeals)
            {
                Label = userMission.dateDay.ToString() + @"\" + userMission.date.Month.ToString(),
                ValueLabel = isDummy == true ? string.Empty : userMission.amountOfMeals.ToString(),
                Color = SKColor.Parse("#00225c"),
                TextColor = SKColor.Parse("#00225c"),
                ValueLabelColor = SKColor.Parse("#00225c"),
            });
            waterDataList.Add(new ChartEntry(userMission.amountOfWater)
            {
                Label = userMission.dateDay.ToString() + @"\" + userMission.date.Month.ToString(),
                ValueLabel = isDummy == true ? string.Empty : userMission.amountOfWater.ToString(),
                Color = SKColor.Parse("#00225c"),
                TextColor = SKColor.Parse("#00225c"),
                ValueLabelColor = SKColor.Parse("#00225c"),
            });
            wightDataList.Add(new ChartEntry(userMission.currentWeight)
            {
                Label = userMission.dateDay.ToString() + @"\" + userMission.date.Month.ToString(),
                ValueLabel = isDummy == true ? string.Empty : userMission.currentWeight.ToString(),
                Color = SKColor.Parse("#00225c"),
                TextColor = SKColor.Parse("#00225c"),
                ValueLabelColor = SKColor.Parse("#00225c"),
            });
        }
        public void  getAllUserDetailsFromServer()
        {
            
            string userName = Intent.GetStringExtra("userName");
            UserMissionTV.Text = userName;
            Console.WriteLine("Hello World!");
            string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/getUserMonthlyMissions?userName=" + userName;
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            string responseAsString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (responseAsString != null)
            {
                JObject obj = JObject.Parse(responseAsString);
                userObj = new User(obj);
            }
        }
    }
}