﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using static Android.Content.ClipData;

namespace Nuriko.Activities
{
    [Activity(Label = "ActivityAdmin")]
    public class ActivityAdmin : AppCompatActivity
    {
        NavigationView navigationView;
        
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.AdminPageNewLAyout);
            // Create your application here
            navigationView = (NavigationView)FindViewById(Resource.Id.navigationViewAdminPlease);
            navigationView.NavigationItemSelected += NavigationView_NavigationItemSelected;
        }

        private void NavigationView_NavigationItemSelected(object sender, NavigationView.NavigationItemSelectedEventArgs e)
        {
            switch (e.MenuItem.ItemId)
            {
                case Resource.Id.SeallUsersItem:
                    {
                        Intent intent = new Intent(this, typeof(Activities.ActivityUsersList));
                        intent.PutExtra("problem", "No");
                        StartActivity(intent);
                        break;
                    }
                case Resource.Id.SeeAllMEssages:
                    {
                        Intent intent = new Intent(this, typeof(Activities.ActivitySeeAllUsersMessages));
                        
                        StartActivity(intent);
                        break;
                    }
                case Resource.Id.USersWithProblem:
                    {

                        Intent intent = new Intent(this, typeof(Activities.ActivityUsersList));
                        intent.PutExtra("problem", "Yes");
                        StartActivity(intent);
                        break;
                    }
               

            }
        
    }
    }
}