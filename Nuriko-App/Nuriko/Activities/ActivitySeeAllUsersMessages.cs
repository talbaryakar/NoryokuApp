﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json.Linq;
using Nuriko.Adapters;
using Nuriko.Classes;
using Nuriko.Fragments;

namespace Nuriko.Activities
{
    [Activity(Label = "ActivitySeeAllUsersMessages")]
    public class ActivitySeeAllUsersMessages : AppCompatActivity
    {
        AdapterUserMessage adapterUserMessages;
        RecyclerView recyclerView;
        List<UserMessage> userMessagesList;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.layoutseeAllUsersMessages);
            recyclerView = (RecyclerView)FindViewById(Resource.Id.userMEssagesRecycleView);
            userMessagesList = getUserMessagesFromServer();
            setupRecyclerView();
        
            void setupRecyclerView()
            {
                recyclerView.SetLayoutManager(new LinearLayoutManager(recyclerView.Context));
                adapterUserMessages = new AdapterUserMessage(userMessagesList);
                adapterUserMessages.ItemClick += AdapterUserMessages_ItemClick;
                adapterUserMessages.callClick += AdapterUserMessages_callClick; ;
                adapterUserMessages.emailClick += AdapterUserMessages_emailClick; ;
                adapterUserMessages.whatsAppClick += AdapterUserMessages_whatsAppClick; ;
                adapterUserMessages.buttonReadClick += AdapterUserMessages_buttonReadClick;

                recyclerView.SetAdapter(adapterUserMessages);
            }
        }

        private void AdapterUserMessages_buttonReadClick(object sender, AdapterUserMessagesClickEventArgs e)
        {
            var message = userMessagesList[e.Position];
            FragmentReadSingleMEssage fragment = new FragmentReadSingleMEssage();
            fragment.body = message.body;
            fragment.subject = message.subject;
            var trans = SupportFragmentManager.BeginTransaction();
            fragment.Show(trans, "wrong");         
        }
        private List<UserMessage> getUserMessagesFromServer()
        {
            List<UserMessage> listOfMessages = new List<UserMessage>();
            string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/getUserMessages";
            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            string responseAsString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (responseAsString != null)
            {
                JArray obj = JArray.Parse(responseAsString);
                foreach (JToken jt in obj)
                {
                    listOfMessages.Add((UserMessage)jt.ToObject(typeof(UserMessage)));
                }
                return listOfMessages;

            }
            return null;
        }
        private void AdapterUserMessages_whatsAppClick(object sender, AdapterUserMessagesClickEventArgs e)
        {
            var message = userMessagesList[e.Position];
            Android.Support.V7.App.AlertDialog.Builder EmailAlert = new Android.Support.V7.App.AlertDialog.Builder(this);
            EmailAlert.SetMessage("Send WhatsApp to :" + message.UserName);
            EmailAlert.SetPositiveButton("Se nd", (alert, args) =>
            {

                if (message.phoneNumber != null && message.phoneNumber.Length > 2)
                {
                    if (message.phoneNumber[0] == '0' && message.phoneNumber[1] == '5')
                    {
                        var whatsaap = Android.Net.Uri.Parse("smsto:" + message.phoneNumber);
                        Intent i = new Intent(Intent.ActionSendto, whatsaap);
                        i.SetPackage("com.whatsapp");
                        StartActivity(Intent.CreateChooser(i, ""));
                    }
                }
            });
            EmailAlert.SetNegativeButton("Cancle", (alert, args) =>
            {
                EmailAlert.Dispose();
            });
            EmailAlert.Show();
        }

        private void AdapterUserMessages_emailClick(object sender, AdapterUserMessagesClickEventArgs e)
        {
            var message = userMessagesList[e.Position];
            Android.Support.V7.App.AlertDialog.Builder EmailAlert = new Android.Support.V7.App.AlertDialog.Builder(this);
            EmailAlert.SetMessage("Send Mail to :" + message.UserName);
            EmailAlert.SetPositiveButton("Send", (alert, args) =>
            {

                Intent intent = new Intent();
                intent.SetType("plain/text");
                intent.SetAction(Intent.ActionSend);
                intent.PutExtra(Android.Content.Intent.ExtraEmail, new string[] { message.gemailAdress });
                intent.PutExtra(Intent.ExtraSubject, "hey its nimrod");
                StartActivity(intent);
            });
            EmailAlert.SetNegativeButton("Cancle", (alert, args) =>
            {
                EmailAlert.Dispose();
            });
            EmailAlert.Show();
        }

        private void AdapterUserMessages_callClick(object sender, AdapterUserMessagesClickEventArgs e)
        {
            var User = userMessagesList[e.Position];
            Android.Support.V7.App.AlertDialog.Builder CallAlerts = new Android.Support.V7.App.AlertDialog.Builder(this);
            CallAlerts.SetMessage("Call " + User.UserName);
            CallAlerts.SetPositiveButton("Call", (alert, args) =>
            {
                var uri = Android.Net.Uri.Parse("tel:" + User.phoneNumber);
                var intent = new Intent(Intent.ActionDial, uri);
                StartActivity(intent);
            });
            CallAlerts.SetNegativeButton("Cancle", (alert, args) =>
            {
                CallAlerts.Dispose();
            });
            CallAlerts.Show();
        }

        private void AdapterUserMessages_ItemClick(object sender, AdapterUserMessagesClickEventArgs e)
        {

            Toast.MakeText(this, "row was clicked", ToastLength.Long).Show();
        }
    }
}