﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Nuriko.Classes
{
    [Serializable]
    class UserMissions
    {
        public int amountOfWater = 0;
        public double targerWeight = 0;
        public int amountOfMeals = 0;
        public bool isSportActivityDone = false;
        public bool isHappinesCircle = false;
        public bool isNewRecord = false;
        public bool isTommorowPlanned = false;
        public bool isCrisisManageSuccufully = false;
        public DateTime date = new DateTime();
        public UserMissions()
        {
            date = DateTime.Now;
        }
    }
}