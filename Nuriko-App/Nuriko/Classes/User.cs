﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json.Linq;

namespace Nuriko.Classes
{
    [Serializable]
    class User
    {
        public string UserName;
        public string password;
        public string emailAdress;
        public string phoneNumber;
        public List<UserMissions> missionsByDate;
        public User(JObject obj)
        {
            
            this.UserName = (string)obj["UserName"];
            this.password = (string)obj["password"];
            this.emailAdress = (string)obj["emailAdress"]; ;
            this.phoneNumber = (string)obj["phoneNumber"]; 
            JToken missions= obj["missionsByDate"];
            this.missionsByDate =(List<UserMissions>) missions.ToObject(typeof(List<UserMissions>));
    }
      
    }
}