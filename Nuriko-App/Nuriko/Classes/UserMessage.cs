﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Nuriko.Classes
{
    [Serializable]
    class UserMessage
    {
        public string timeStamp;
        public string UserName;
        public string gemailAdress;
        public string phoneNumber;
        public string subject;
        public string body;
    }
}