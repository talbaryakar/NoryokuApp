﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Nuriko.Fragments
{
    public delegate void sendMessgaeToAdminDelegate(string a ,string b);
    public class FragmentSendMessageToADmin : Android.Support.V4.App.DialogFragment
    {
        public sendMessgaeToAdminDelegate sendMessgaeToAdminDelegate;
        View view;
        Button submit;
        EditText subject, bodyMessage;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            view = inflater.Inflate(Resource.Layout.layoutSendMessageToAdmin, container, false);
            submit = (Button)view.FindViewById(Resource.Id.ButtonSubmitSendMessage);
            subject = (EditText)view.FindViewById(Resource.Id.EditTextSubjectLayout);
            bodyMessage= (EditText)view.FindViewById(Resource.Id.EditTextBodytLayout);
            submit.Click += Submit_Click;
            return view;
        }

        private void Submit_Click(object sender, EventArgs e)
        {

            sendMessgaeToAdminDelegate?.Invoke(subject.Text, bodyMessage.Text);
            this.Dismiss();
        }

        
    }
}