﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Nuriko.Fragments
{
    public delegate void delegateUpdateWeight(int dest, int current);
    public class FragmentWeight : Android.Support.V4.App.DialogFragment
    {
        public delegateUpdateWeight updateWeightDelegate;
        public int destinationWeight, currentWeight;
        Button buttonSubmit;
        SeekBar seekBArCurrent, SeekBarDest;
        TextView currentTextView, DestTextView;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            View view= inflater.Inflate(Resource.Layout.layoutWeight, container, false);
            buttonSubmit = (Button)view.FindViewById(Resource.Id.ButtonWeightSubmit);
            buttonSubmit.Click += ButtonSubmit_Click;
            currentTextView = (TextView)view.FindViewById(Resource.Id.TextViewCurrentWeightSum);
            DestTextView = (TextView)view.FindViewById(Resource.Id.TextViewDestWeight);
            seekBArCurrent = (SeekBar)view.FindViewById(Resource.Id.seekBarCurrentWeight);
            SeekBarDest = (SeekBar)view.FindViewById(Resource.Id.seekBarWeight);
            seekBArCurrent.ProgressChanged += SeekBArCurrent_ProgressChanged;
            SeekBarDest.ProgressChanged += SeekBarDest_ProgressChanged;
            setComponents(currentWeight, destinationWeight);
            return view;
        }

        private void ButtonSubmit_Click(object sender, EventArgs e)
        {
            int current,destination;
            if (int.TryParse(currentTextView.Text, out current))
            {
          
            }
            else
            {
                current = 0;
            }
            if (int.TryParse(DestTextView.Text, out destination))
            {
            }
            else
            {
                destination = 0;
            }
            updateWeightDelegate?.Invoke(destination, current);
            this.Dismiss();
        }

        public void setComponents(int currnet , int dest)
        {
            if(currnet>0)
            {
                seekBArCurrent.Progress = currnet;
                currentTextView.Text = currnet.ToString();
            }
            else
            {
                currentTextView.Text = "לא עודכן";
            }
            if (dest > 0)
            {
                DestTextView.Text = dest.ToString();
                SeekBarDest.Progress = dest;
            }
            else
            {
                DestTextView.Text = "לא עודכן";
            }
        }
        private void SeekBarDest_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            DestTextView.Text = SeekBarDest.Progress.ToString();
        }

        private void SeekBArCurrent_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            currentTextView.Text = seekBArCurrent.Progress.ToString();
        }
    }
}