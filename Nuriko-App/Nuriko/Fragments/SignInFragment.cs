﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nuriko.Classes;

namespace Nuriko.Fragments
{
    public delegate void OpenSignUpPage();
    public delegate void OpenMainMenuForUser(string user);
    public class SignInFragment : Android.Support.V4.App.DialogFragment
    {
        public OpenMainMenuForUser notifierMainMenuUser;
        public OpenSignUpPage notifier;
        View view;
        Button Enter;
        EditText userName, Password;
        LinearLayout createNewAccount;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Create your fragment here
        }

        private void CreateNewAccount_Click(object sender, EventArgs e)
        {
            notifier?.Invoke();
            this.Dismiss();
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
           view = inflater.Inflate(Resource.Layout.SignInLayout, container, false);
            userName = (EditText)view.FindViewById(Resource.Id.EditTextUserNameSignInLAyout);
            Password = (EditText)view.FindViewById(Resource.Id.EditTextPasswordSignInLayout);
            createNewAccount = (LinearLayout)view.FindViewById(Resource.Id.LinearLayoutNoAccount);
            Enter = (Button)view.FindViewById(Resource.Id.ButtonEnterSignInLayout);
            Enter.Click += Enter_Click;
            createNewAccount.Click += CreateNewAccount_Click;
            return view;
        }

        private void Enter_Click(object sender, EventArgs e)
        {
            Console.WriteLine("Hello World!");
            string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/checkIfUserDetailsCorrectAndReturnUser?password=" + Password.Text + "&userName=" + userName.Text;


            var request = (HttpWebRequest)WebRequest.Create(url);
            var response = (HttpWebResponse)request.GetResponse();
            string responseAsString = new StreamReader(response.GetResponseStream()).ReadToEnd();
            if (responseAsString != null)
                notifierMainMenuUser?.Invoke(responseAsString);
            else
            {
                Toast.MakeText(view.Context, "שם משתמש וסיסמא אינם נכונים", ToastLength.Long).Show();
            }



        }

    }
}


/*
 *  string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/checkIfUserDetailsCorrectAndReturnUser?password="+ Password.Text+ "&userName="+userName.Text;
 */