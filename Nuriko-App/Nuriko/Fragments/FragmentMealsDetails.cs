﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Nuriko.Fragments
{
    public delegate void setAmountOfMeals(int amountOfMeals);
    public class FragmentMealsDetails : Android.Support.V4.App.DialogFragment
    {
        public setAmountOfMeals amountOfMealsDelegate;
        private Button submmit;
        private SeekBar seekBar;
        private TextView amountOfWater;
        View view;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            view = inflater.Inflate(Resource.Layout.MealsDetailsLayout, container, false);
            submmit = (Button)view.FindViewById(Resource.Id.ButtonMealsLayoutAmountSubmmit);
            seekBar = (SeekBar)view.FindViewById(Resource.Id.seekBarMeals);
            amountOfWater = (TextView)view.FindViewById(Resource.Id.TextViewNumberOfMeals);
            seekBar.ProgressChanged += SeekBar_ProgressChanged;
            submmit.Click += Submmit_Click;

            return view;
        }

        private void Submmit_Click(object sender, EventArgs e)
        {
            amountOfMealsDelegate?.Invoke(int.Parse(amountOfWater.Text));
            this.Dismiss();
        }

        private void SeekBar_ProgressChanged(object sender, SeekBar.ProgressChangedEventArgs e)
        {
            amountOfWater.Text = seekBar.Progress.ToString();
        }
    }
}