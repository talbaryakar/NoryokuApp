﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Nuriko.Fragments
{
    public class FragmentReadSingleMEssage : Android.Support.V4.App.DialogFragment
    {
        View view;
        public string subject, body;
        TextView subjectTV, bodyTV;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
             view = inflater.Inflate(Resource.Layout.layoutSingleMEssage, container, false);
            subjectTV = (TextView)view.FindViewById(Resource.Id.subjectMessageId);
            bodyTV  = (TextView)view.FindViewById(Resource.Id.bodyMessageId);
            subjectTV.Text = subject;
            bodyTV.Text = body;
            return view;
        }
    }
}