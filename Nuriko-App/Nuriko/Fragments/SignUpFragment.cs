﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Nuriko.Fragments
{
    
    public class SignUpFragment : Android.Support.V4.App.DialogFragment
    {
        
        View view;
        Button buttonClick;
        TextView userName, Password, Email, Phone;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            // Create your fragment here
        }

        private void ButtonClick_Click(object sender, EventArgs e)
        {
            string userNAmeStr = userName.Text;
            string passwordStr = Password.Text;
            string EmailStr = Email.Text;
            string phoneStr = Phone.Text;
            string url = "https://webapplicationnuirko.azurewebsites.net/USerDetails/addNewUserToSystem";

            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            string contents = string.Empty;
            contents += userNAmeStr + "|";
            contents+=passwordStr + "|";
            contents+=EmailStr + "|";
            contents+=phoneStr;
            byte[] bytes = Encoding.UTF8.GetBytes(contents);
            request.ContentLength = bytes.Length;
            using (Stream requestStream = request.GetRequestStream())
            {
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Flush();
                requestStream.Close();
                using (WebResponse response = request.GetResponse())
                {
                    using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                    {
                        string line = reader.ReadToEnd();
                        if(line==null || line =="" || line==string.Empty)
                        {
                            Toast.MakeText(view.Context, "שם משתמש קיים במערכת ", ToastLength.Long).Show();
                        }
                        else
                        {
                            Toast.MakeText(view.Context, "נוסף משתמש חדש  ", ToastLength.Long).Show();
                        }
                    }


                }
            }
        }
        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            view = inflater.Inflate(Resource.Layout.SignUpLayout, container, false);
            buttonClick = (Button)view.FindViewById(Resource.Id.ButtonSignUpSignUpLayout);
            userName = (TextView)view.FindViewById(Resource.Id.EditTextUserNameSignUpLayout);
            Password = (TextView)view.FindViewById(Resource.Id.passwordSignUpLayout);
            Email = (TextView)view.FindViewById(Resource.Id.emailSignUpLayout);
            Phone = (TextView)view.FindViewById(Resource.Id.PhoneSignUpLayout);
            buttonClick.Click += ButtonClick_Click;
            return view;
        }
    }
}