﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace Nuriko.Fragments
{
    public class SignInAsAdministerFragment : Android.Support.V4.App.DialogFragment
    {
        View view;
       
        Button Enter;
        EditText userName, Password;
        LinearLayout createNewAccount;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);

            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
            view = inflater.Inflate(Resource.Layout.SignInLayout, container, false);
            userName = (EditText)view.FindViewById(Resource.Id.EditTextUserNameSignInLAyout);
            Password = (EditText)view.FindViewById(Resource.Id.EditTextPasswordSignInLayout);
            createNewAccount = (LinearLayout)view.FindViewById(Resource.Id.LinearLayoutNoAccount);
            Enter = (Button)view.FindViewById(Resource.Id.ButtonEnterSignInLayout);
            createNewAccount.Visibility = ViewStates.Invisible; 
            return view;

        }
    }
}