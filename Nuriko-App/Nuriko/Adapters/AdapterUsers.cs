﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Nuriko.Classes;
using System.Collections.Generic;

namespace Nuriko.Adapters
{
    class AdapterUsers : RecyclerView.Adapter
    {
        public event EventHandler<AdapterUsersClickEventArgs> ItemClick;
        public event EventHandler<AdapterUsersClickEventArgs> ItemLongClick;
        public event EventHandler<AdapterUsersClickEventArgs> callClick;
        public event EventHandler<AdapterUsersClickEventArgs> emailClick;
        public event EventHandler<AdapterUsersClickEventArgs> whatsAppClick;
        public event EventHandler<AdapterUsersClickEventArgs> displayHistoryClick;

        List<User> items;

        public AdapterUsers(List<User> data)
        {
            items = data;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            //var id = Resource.Layout.__YOUR_ITEM_HERE;
            //itemView = LayoutInflater.From(parent.Context).
            //       Inflate(id, parent, false);
            itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.userRowLayout, parent, false);
            var vh = new AdapterUsersViewHolder(itemView, OnClick, OnLongClick, onCallClick,onEmailClick,onWhatsAppClick,onDisplayHistoryClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as AdapterUsersViewHolder;
            holder.userNameTextView.Text = item.UserName;
            
           
        }

        public override int ItemCount => items.Count;

        void OnClick(AdapterUsersClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(AdapterUsersClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        void onCallClick(AdapterUsersClickEventArgs args) => callClick?.Invoke(this, args);
        void onEmailClick(AdapterUsersClickEventArgs args) => emailClick?.Invoke(this, args);
        void onWhatsAppClick(AdapterUsersClickEventArgs args) => whatsAppClick?.Invoke(this, args);

        void onDisplayHistoryClick(AdapterUsersClickEventArgs args) => displayHistoryClick?.Invoke(this, args);
    }

    public class AdapterUsersViewHolder : RecyclerView.ViewHolder
    {
        //public TextView TextView { get; set; }
        public TextView userNameTextView;
        public RelativeLayout email, call, whatsapp, seeHidtoryData;

        public AdapterUsersViewHolder(View itemView, Action<AdapterUsersClickEventArgs> clickListener,
                            Action<AdapterUsersClickEventArgs> longClickListener, Action<AdapterUsersClickEventArgs> callClickListener
            , Action<AdapterUsersClickEventArgs> emailClickListener, Action<AdapterUsersClickEventArgs> whatsAppClickListener, Action<AdapterUsersClickEventArgs> displaYhistoryData) : base(itemView)
        {
            //TextView = v;
             userNameTextView = (TextView)itemView.FindViewById(Resource.Id.userNAmeTextView); 
             email = (RelativeLayout)itemView.FindViewById(Resource.Id.EmailIdLAyoutAdminPage);

            call = (RelativeLayout)itemView.FindViewById(Resource.Id.CallIdLayoutAdminPAge);
            email.Click+=(sender, e)=>emailClickListener(new AdapterUsersClickEventArgs {View = itemView, Position=AdapterPosition });

            call.Click += (sender, e) => callClickListener(new AdapterUsersClickEventArgs { View = itemView, Position = AdapterPosition });
            
            whatsapp = (RelativeLayout)itemView.FindViewById(Resource.Id.whatsAppLayoutAdmin);
            whatsapp.Click += (sender, e) => whatsAppClickListener(new AdapterUsersClickEventArgs { View = itemView, Position = AdapterPosition });

            seeHidtoryData = (RelativeLayout)itemView.FindViewById(Resource.Id.displayMonthlyMissions);
            seeHidtoryData.Click += (sender, e) => displaYhistoryData(new AdapterUsersClickEventArgs { View = itemView, Position = AdapterPosition });

            itemView.Click += (sender, e) => clickListener(new AdapterUsersClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new AdapterUsersClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class AdapterUsersClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}