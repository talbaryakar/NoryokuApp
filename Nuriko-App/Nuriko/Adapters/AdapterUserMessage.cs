﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Nuriko.Classes;

namespace Nuriko.Adapters
{
    class AdapterUserMessage : RecyclerView.Adapter
    {
        public event EventHandler<AdapterUserMessagesClickEventArgs> ItemClick;
        public event EventHandler<AdapterUserMessagesClickEventArgs> ItemLongClick;
        public event EventHandler<AdapterUserMessagesClickEventArgs> callClick;
        public event EventHandler<AdapterUserMessagesClickEventArgs> emailClick;
        public event EventHandler<AdapterUserMessagesClickEventArgs> whatsAppClick;
        public event EventHandler<AdapterUserMessagesClickEventArgs> buttonReadClick;
        List<UserMessage> items;

        public AdapterUserMessage(List<UserMessage> data)
        {
            items = data;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = null;
            //var id = Resource.Layout.__YOUR_ITEM_HERE;
            //itemView = LayoutInflater.From(parent.Context).
            //       Inflate(id, parent, false);
            itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.layoutUserMessageRow, parent, false);
            var vh = new AdapterUserMessageViewHolder(itemView, OnClick, OnLongClick, onCallClick, onEmailClick, onWhatsAppClick, onReadButtonClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as AdapterUserMessageViewHolder;
            holder.userNameTextView.Text = item.UserName;


        }

        public override int ItemCount => items.Count;

        void OnClick(AdapterUserMessagesClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(AdapterUserMessagesClickEventArgs args) => ItemLongClick?.Invoke(this, args);
        void onCallClick(AdapterUserMessagesClickEventArgs args) => callClick?.Invoke(this, args);
        void onEmailClick(AdapterUserMessagesClickEventArgs args) => emailClick?.Invoke(this, args);
        void onWhatsAppClick(AdapterUserMessagesClickEventArgs args) => whatsAppClick?.Invoke(this, args);
        void onReadButtonClick(AdapterUserMessagesClickEventArgs args) => buttonReadClick?.Invoke(this, args);
    }

    public class AdapterUserMessageViewHolder : RecyclerView.ViewHolder
    {
        public TextView userNameTextView;
        public RelativeLayout email, call, whatsapp;
        Button buttonReadMessage;
        public AdapterUserMessageViewHolder(View itemView, Action<AdapterUserMessagesClickEventArgs> clickListener,
                             Action<AdapterUserMessagesClickEventArgs> longClickListener, Action<AdapterUserMessagesClickEventArgs> callClickListener
             , Action<AdapterUserMessagesClickEventArgs> emailClickListener, Action<AdapterUserMessagesClickEventArgs> whatsAppClickListener, Action<AdapterUserMessagesClickEventArgs> buttonReadClickListener ) : base(itemView)
        {
            //TextView = v;
            userNameTextView = (TextView)itemView.FindViewById(Resource.Id.userNameTextViewUserMessages);
            email = (RelativeLayout)itemView.FindViewById(Resource.Id.EmailIdLAyoutUsersMEssages);

            call = (RelativeLayout)itemView.FindViewById(Resource.Id.CallIdLayoutUsersMessages);
            email.Click += (sender, e) => emailClickListener(new AdapterUserMessagesClickEventArgs { View = itemView, Position = AdapterPosition });

            call.Click += (sender, e) => callClickListener(new AdapterUserMessagesClickEventArgs { View = itemView, Position = AdapterPosition });

            whatsapp = (RelativeLayout)itemView.FindViewById(Resource.Id.whatsAppLayoutUserMessages);
            whatsapp.Click += (sender, e) => whatsAppClickListener(new AdapterUserMessagesClickEventArgs { View = itemView, Position = AdapterPosition });

            itemView.Click += (sender, e) => clickListener(new AdapterUserMessagesClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new AdapterUserMessagesClickEventArgs { View = itemView, Position = AdapterPosition });

            buttonReadMessage = (Button)itemView.FindViewById(Resource.Id.buttonReadMessages);
            buttonReadMessage.Click+= (sender, e) => buttonReadClickListener(new AdapterUserMessagesClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }
    public class AdapterUserMessagesClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}